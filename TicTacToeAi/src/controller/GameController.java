package controller;

import java.awt.Point;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import controller.FieldController.playerType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import model.Gamer;
import model.Stone;

public class GameController 
{
	
	private Stone[][] gameArray;
	
	public Stone[][] getGameArray()
	{
		return this.gameArray;
	}
	
	public void setGameArray(Stone[][] gameArray)
	{
		this.gameArray = gameArray;
	}
	
	public GameController(playerType startingPlayer)
	{
		this.currentTurn = startingPlayer;
		this.gameArray = new Stone[3][3];
	}

	private playerType currentTurn;
	
	public playerType getCurrentTurn()
	{
		return this.currentTurn;
	}
	
	public void setCurrenTurn(playerType currentTurn)
	{
		this.currentTurn = currentTurn;
	}
	
	public boolean drawStone(ImageView view, Gamer gamer)
	{
		if(gamer.getPlayerType() == currentTurn)
		{
			// Check if ImageView is available
			int xPos = xPosParser(view.getId());
			int yPos = yPosParser(view.getId());
			
			for(int i = 0; i < gameArray.length; i++)
			{
				for(int j = 0 ; j < gameArray.length; j++)
				{
					if(gameArray[xPos][yPos] == null)
					{
						view.setImage(gamer.getPlayerImage());
						refreshArray(view);
						displayGameArray();
						changeCurrentTurn();
						return true;
					}
					else
					{
						return false;
					}
				}
			}
		}
		return false;
	}
	
	public boolean checkForEmptyPlace()
	{
	
		for(int i = 0; i < gameArray.length; i++)
		{
			for(int j = 0; j < gameArray.length; j++)
			{
				if(gameArray[i][j] == null)
				{
					return true;
				}
			}
		}
		return false;
	}
	
	private void changeCurrentTurn()
	{
		if(currentTurn == playerType.Player1)
		{
			currentTurn = playerType.AI;
		}
		else if(currentTurn == playerType.AI)
		{
			currentTurn = playerType.Player1;
		}
	}
	
	public void refreshArray(ImageView imageView)
	{
		// example imageView00
		String tempName = imageView.getId();
		int xPosition = xPosParser(tempName);
		int yPosition = yPosParser(tempName);
		
		Stone stone = new Stone();
		stone.setImageView(imageView);
		stone.setPlayer(getCurrentTurn());
			
		gameArray[xPosition][yPosition] = stone;
	}
	
	private int xPosParser(String name)
	{
		if(name.length() == 11)
		{
			String xTemp = "" + name.charAt(9);
			int xPosition = Integer.parseInt(xTemp); // X- Position 
			return xPosition;
		}
		return -1;
	}
	
	private int yPosParser(String name)
	{
		if(name.length() == 11)
		{
			String yTemp = "" + name.charAt(10);
			int yPosition = Integer.parseInt(yTemp);
			return yPosition;
		}
		
		return -1;
	}
	
	public void displayGameArray()
	{
		for(int i = 0; i < gameArray.length; i++)
		{
			for(int j = 0; j < gameArray.length; j++)
			{
				System.out.print(gameArray[i][j] + " -- ");
			}
			System.out.println("");
		}
	}

}
