package controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import model.Point;
import controller.FieldController.playerType;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import model.Gamer;
import model.Stone;

public class KiController 
{

	private GameController gameController;
	private GridPane gridPaneGame;
	private Gamer gamerKi;
	private ImageView imageView;
	private AlgoBoard board;

	public KiController(GameController gameController, GridPane gridPaneGame, Gamer gamerKi) 
	{
		this.gameController = gameController;
		this.gridPaneGame = gridPaneGame;
		this.gamerKi = gamerKi;
		this.board = new AlgoBoard(this.gameController.getGameArray());
		
	}
	
	public Gamer getGamer()
	{
		return this.gamerKi;
	}
	
	public AlgoBoard getAlgoBoard()
	{
		return this.board;
	}

	/*public void checkFieldStatus(int level) throws Exception
	{
		System.out.println("Looks stupid");
		// Check if there is an empty place for another Stone
		if (this.gameController.checkForEmptyPlace() == true)
		{
			// Check selected level
			switch (level)
			{
			case 1:
				this.level = 1;
				break;
			case 2:
				this.level = 2;
				break;
			default:
				break;
			}
		}
	}*/

	public ImageView level1Random() 
	{
		Random rnd = new Random();
		while (true) 
		{
			int randomNumber = rnd.nextInt(8);
			// Check if randomNumber is already in use
			ImageView tempImageView = (ImageView) gridPaneGame.getChildren().get(randomNumber);

			if (tempImageView.getImage() == null) 
			{
				return tempImageView;
			}
		}
	}
	
	public ImageView level2minimax() throws Exception
	{	
		board.minimax(0, 1);
		
		System.out.println("Computer choose position : " + board.computerMove);
		return createImageView(imageView, board.computerMove);
	}

	private ImageView createImageView(ImageView imageView, Point point) throws Exception
	{
		String id;
		id = "imageView" + point.x + "" + point.y;
		
		ObservableList<Node> gridPaneGameList = this.gridPaneGame.getChildren();
		for(int i = 0; i < gridPaneGameList.size(); i++)
		{
			if(gridPaneGameList.get(i) != null)
			{
				ImageView node = (ImageView) gridPaneGameList.get(i);
				if(node.getId().equals(id))
				{
					return node;
				}
			}
		}
		
		throw new Exception("Error createImageView!");
	}
}
