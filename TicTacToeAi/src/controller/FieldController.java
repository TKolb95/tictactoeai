package controller;

import java.awt.Dialog;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.scene.Node;
import javafx.scene.Parent;
import application.Main;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuItem;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import model.Gamer;

public class FieldController implements Initializable
{
    @FXML
    private GridPane gridPaneGame;

    @FXML
    private ImageView imageView00;

    @FXML
    private ImageView imageView10;

    @FXML
    private ImageView imageView20;

    @FXML
    private ImageView imageView01;

    @FXML
    private ImageView imageView11;

    @FXML
    private ImageView imageView21;

    @FXML
    private ImageView imageView02;

    @FXML
    private ImageView imageView12;

    @FXML
    private ImageView imageView22;
    
    @FXML
    private MenuItem menuItemClose;
    
    @FXML
    private MenuItem menuItemSettings;
    
    @FXML
    private MenuItem menuItemRestart;
    
    @FXML
    private MenuItem menuHelp;
    
    @FXML
    private Label labelOutput;
    
    private Scene gameScene;
    
    private Scene settingsScene;
    
    private Stage stage;
    
    public void setStage(Stage stage)
    {
    		this.stage = stage;
    		this.stage.setScene(gameScene);
    }
    
    public Stage getStage()
    {
    		return this.stage;
    }
    
    public void setSettingsScene(Scene scene)
    {
    		this.settingsScene = scene;
    }
    
    public Scene getSettingsScene(Scene scene)
    {
    		return this.settingsScene;
    }
    
    public void setGameScene(Scene scene)
    {
    		this.gameScene = scene;
    }
    
    public Scene getGameScene()
    {
    		return this.gameScene;
    }
    
    private SettingsController sController;
    
    public void setSettingsController(SettingsController sController)
    {
    		this.sController = sController;
    }
    
    public SettingsController getSettingsController()
    {
    		return this.sController;
    }
    
    @FXML
    void onActionHelp(ActionEvent event)
    {
    		
    }
    
    @FXML
    void onActionSave(ActionEvent event)
    {
    	
    }
    
    @FXML
    void onActionSettings(ActionEvent event)
    {
    		Main.game = true;
   		Main.getStage().close();
		Platform.runLater(() -> new Main().start(new Stage()));
    }
    
    @FXML
    void onActionRestart(ActionEvent event) throws InterruptedException, IOException
    {
    		Main.game = false;	
    		Main.getStage().close();
    		Platform.runLater(() -> new Main().start(new Stage()));
    }

    @FXML
    void onActionClose(ActionEvent event) 
    {
    		System.exit(0);
    }

	public enum playerType { Player1, AI, Nothing};
	private 	boolean wasTurnSuccessfull = false;
	
	@Override
	public void initialize(URL location, ResourceBundle resources) 
	{
		
		
		// Get Name of the Gamer
		 Gamer gamer = new Gamer("", new Image("./imageO.png"), playerType.Player1);
		 Gamer gamerki = new Gamer("AI", new Image("./imageX.png"), playerType.AI);
		 
		 
		GameController gameController = new GameController(playerType.Player1);
		KiController kiController = new KiController(gameController, gridPaneGame, gamerki);
	
		
		ObservableList<ImageView> imageViews = FXCollections.observableArrayList();
		imageViews.addAll(imageView00, imageView01, imageView02
						, imageView10, imageView11, imageView12
						, imageView20, imageView21, imageView22);
	
		for(ImageView view : imageViews)
		{
			view.setOnMouseClicked(new EventHandler<MouseEvent>() 
			{
				@Override
				public void handle(MouseEvent event) 
				{

					gamer.setName(Main.gamerName);
					
					if(gameController.getCurrentTurn() == gamer.getPlayerType())
					{
						
						System.out.println("Playername: " + Main.gamerName);
						System.out.println("Level: " + Main.level);
						System.out.println("MouseClicked! : " + view.getId());

						wasTurnSuccessfull = gameController.drawStone(view, gamer);
						
						if(wasTurnSuccessfull == false)
						{
							labelOutput.setText("Dieses Feld ist bereits belegt. Bitte wählen Sie ein anderes aus");
						}
						
						if(wasTurnSuccessfull == true)
						{
							AlgoBoard board = kiController.getAlgoBoard();
							labelOutput.setText("MouseClicked: " + view.getId());
							try 
							{
								ImageView imageView;
								//kiController.checkFieldStatus(level);
								switch(Main.level)
								{
									case 1: imageView = kiController.level1Random();
											break;
									case 2: imageView = kiController.level2minimax();
											break;
									default: throw new Exception("ERROR: No ImageView - kiController - Level ");
								}
								
								if(imageView != null)
								{
									gameController.drawStone(imageView, kiController.getGamer());
									board = kiController.getAlgoBoard();
									playerType gamerWon = board.isGameOver();
									if(gamerWon != null && gamerWon != playerType.Nothing)
									{
										// Spiel abbrechen - Gewinner steht fest.
										System.out.println(gamerWon);
										Alert alert = new Alert(AlertType.INFORMATION,"Nochmal?",ButtonType.YES, ButtonType.NO, ButtonType.CLOSE);
										alert.setTitle("Spiel beendet");
										alert.setHeaderText(gamerWon + " hat das Spiel gewonnen!");
										alert.showAndWait();
										if(alert.getResult() == ButtonType.YES)
										{
											Main.game = false;
								    			Main.getStage().close();
								    			Platform.runLater(() -> new Main().start(new Stage()));
										}
										else if(alert.getResult() == ButtonType.CLOSE)
										{
											System.exit(0);
										}
										
									}
									else if(gamerWon != null && gamerWon == playerType.Nothing)
									{
										System.out.println(gamerWon);
										Alert alert = new Alert(AlertType.INFORMATION,"Nocheinmal? ", ButtonType.YES, ButtonType.NO, ButtonType.CLOSE);
										alert.setTitle("Spiel beendet");
										alert.setHeaderText("Unentschieden.");
										alert.showAndWait();
										
										if(alert.getResult() == ButtonType.YES)
										{
								    			Main.getStage().close();
								    			Main.game = false;
								    			Main.gamerName = sController.getNameLabel().getText();
								    			Platform.runLater(() -> new Main().start(new Stage()));
										}
										else if(alert.getResult() == ButtonType.CLOSE)
										{
											System.exit(0);
										}
									}
									
								}
							} 
							catch (Exception e) 
							{
								System.out.println("Error in kiController");
								e.printStackTrace();
							}
						}
					}
				}
			});
		}
	}

}
