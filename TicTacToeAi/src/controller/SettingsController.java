package controller;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import application.Main;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class SettingsController implements Initializable
{
    @FXML
    private TextField nameLabel;
    
    public TextField getNameLabel()
    {
    		return this.nameLabel;
    }
    
    public static String gamerName;

    @FXML
    private RadioButton rbtLevel1;

    @FXML
    private RadioButton rbtLevel2;

    @FXML
    private Button btnStart;

    @FXML
    private Button btnCancel;
    
    private Scene gameScene;
    
    public void setGameScene(Scene scene)
    {
    		this.gameScene = scene;
    }
    
    public Scene getGameScene()
    {
    		return this.gameScene;
    }
    
    private Stage stage;
    
    public void setStage(Stage stage)
    {
    		this.stage = stage;
    }
    
    public Stage getStage()
    {
    		return stage;
    }
    
	@Override
	public void initialize(URL location, ResourceBundle resources)
	{
		ToggleGroup btnGroup = new ToggleGroup();
		rbtLevel1.setToggleGroup(btnGroup);
		rbtLevel2.setToggleGroup(btnGroup);
		rbtLevel1.setSelected(true);
		
		btnStart.setOnAction(new EventHandler<ActionEvent>()
		{
			@Override
			public void handle(ActionEvent event)
			{
					if(!(nameLabel.getText().isEmpty()))
					{
						FXMLLoader loader = new FXMLLoader(getClass().getResource("/views/Playground.fxml"));
						Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
						stage.setScene(gameScene);
						Main.gamerName = nameLabel.toString();
						if(rbtLevel1.isSelected())
						{
							Main.level = 1;
						}
						else
						{
							Main.level = 2;
						}
					}
					else
					{
						Alert alert = new Alert(AlertType.ERROR);
						alert.setTitle("Fehler!");
						alert.setHeaderText("Es wurde kein Name ausgewählt");
						alert.showAndWait();
					}
			}
		});
		
		btnCancel.setOnAction(new EventHandler<ActionEvent>() 
		{
			@Override
			public void handle(ActionEvent event) 
			{
				System.exit(0);
			}
		});
	}

}
