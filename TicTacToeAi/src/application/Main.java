package application;
	
import controller.FieldController;
import controller.SettingsController;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.fxml.FXMLLoader;


public class Main extends Application {
	
	private static Stage stage;
	
	public static int level;
	
	public static String gamerName;
	
	public static Stage getStage()
	{
		return stage;
	}
	
	public static boolean game = true;
	
	@Override
	public void start(Stage primaryStage) {
		try {
			if(game == true)
			{
				BorderPane root = new BorderPane();
				FXMLLoader settingsLoader = new FXMLLoader(getClass().getResource("/views/Settings.fxml"));
				Parent settingsPane = settingsLoader.load();
				
				Scene settingsScene = new Scene(settingsPane);
				
				FXMLLoader gameLoader = new FXMLLoader(getClass().getResource("/views/Playground.fxml"));
				Parent gamePane = gameLoader.load();
				
				Scene gameScene = new Scene(gamePane);
				
				SettingsController sController = (SettingsController) settingsLoader.getController();
				sController.setGameScene(gameScene);
		
				FieldController fController = (FieldController) gameLoader.getController();
				
				fController.setSettingsController(sController);
				fController.setSettingsScene(settingsScene);
				fController.setGameScene(gameScene);
				fController.setStage(primaryStage);
				
				root.setCenter(settingsPane);
				Scene scene = new Scene(root);
				stage = primaryStage;
				
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.setResizable(false);
				primaryStage.show();
			}
			else
			{
				BorderPane root = new BorderPane();
				FXMLLoader gameLoader = new FXMLLoader(getClass().getResource("/views/Playground.fxml"));
				Parent gamePane = gameLoader.load();
				
				Scene gameScene = new Scene(gamePane);
				root.setCenter(gamePane);
				Scene scene = new Scene(root);
				stage = primaryStage;
				scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
				primaryStage.setScene(scene);
				primaryStage.setResizable(false);
				primaryStage.show();
			}
		} 
		catch(Exception e) 
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		launch(args);
	}
}
