package model;

import controller.FieldController.playerType;
import javafx.scene.image.ImageView;

public class Stone 
{
	public Stone()
	{
		
	}
	
	private ImageView imageView;
	private playerType player;
	
	public void setImageView(ImageView imageView)
	{
		this.imageView = imageView;
	}
	
	public ImageView getImageView()
	{
		return this.imageView;
	}
	
	public void setPlayer(playerType player)
	{
		this.player = player;
	}
	
	public playerType getPlayer()
	{
		return this.player;
	}
}
