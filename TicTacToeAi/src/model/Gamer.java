package model;

import controller.FieldController.playerType;
import controller.FieldController;
import javafx.scene.image.Image;

public class Gamer 
{
	public Gamer(String name, Image playerImage, playerType playerType)
	{
		this.name = name;
		this.playerType  = playerType;
		this.playerImage = playerImage;
	}
	
	private Image playerImage;
	
	public Image getPlayerImage() {
		return playerImage;
	}

	public void setPlayerImage(Image playerImage) {
		this.playerImage = playerImage;
	}

	private playerType playerType;
	
	public playerType getPlayerType() {
		return playerType;
	}

	public void setPlayerType(playerType playerType) {
		this.playerType = playerType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getWins() {
		return wins;
	}

	public void setWins(int wins) {
		this.wins = wins;
	}

	public int getLose() {
		return lose;
	}

	public void setLose(int lose) {
		this.lose = lose;
	}

	private String name;
	
	private int wins;
	
	private int lose;
	

	
	
	
}
